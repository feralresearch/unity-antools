# An's Unity Tools
## v.α0w0
**USE AT YOUR OWN RISK.   
ALWAYS MAKE A BACKUP OF YOUR PROJECT BEFORE DOING ANYTHING AT ALL, EVER.**

## Install
- Put the scripts in your unity project (you can clone the entire git repo and just drag the folder into assets. Maybe someday I'll get fancy and make a Unity package)
- You should now have `Tools/AnTools` which will bring up a Unity panel. 

## BlendShape Values
Supports copying, exporting and importing of BlendShape values.

- Used to copy values from SOURCE to DESTINATION. 

- Both must be objects with BlendShapes, unless: 

- If "Apply to Children" is checked, then the destination may be a parent object, in which case the source values will be copied to all matching BlendShapes on all of the children of the specified object. Will not go deeper than one level.

- Values may be exported or imported to JSON format

- Destination may be ZEROED which sets all values to zero.


## Armature Merge
The Unity-preferred way to support multiple meshes following the same avatar is to use parent constraints. In VRC specifically, this causes some strange issues, especially with mirrors. The "preferred" ~~method~~ hack is to copy the child armature bone-by-bone into the avatar's main armature such that each bone on the avatar contains the bones of the mesh you want to follow (`Head` on the av contains `Head` of the clothing, for instance). This is a huge pain to set up and undo, this tool addresses that. 

- Source and Destination must NOT be prefabs (unpack them first).

- Set the Source and the Destination and click MERGE. This is a destructive operation, it will effectively move the Source armature into position, removing it from the previous location and deleting the parent.

- CLEAN will remove nodes from the DESTINATION armature whose parent matches on name, effectively reversing the previous Merge operation (but not restoring the original! )



## Thank you!
- Many thanks to [Roliga](https://github.com/Roliga/unity-utils) for putting up with all of my questions and clearing up more than a few VRC/Unity mysteries.

- [Pumpkin's Avatar Tools](https://github.com/rurre/PumkinsAvatarTools) didn't quite handle the BlendShape copy the way I wanted it to, but it offers many more features than mine and you should check it out if you're doing avatar work for VRC!