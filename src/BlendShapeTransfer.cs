﻿#if (UNITY_EDITOR)
using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.IO;

namespace An
{
    [Serializable]
    public class ShapeKey
    {
        public string name;
        public float value;
    }

    [Serializable]
    public class RootObject
    {
        public ShapeKey[] shapeKeys;
    }

    public static class BlendShapeTransfer
    {
        public static void ImportJson(GameObject obj)
        {
            string gamePath = An.Util.GetGameObjectPath(obj);
            if (!obj.TryGetComponent<SkinnedMeshRenderer>(out var skinnedMeshRendererDst))
            {
                Debug.LogWarning(gamePath + " doesn't contain blend shapes!");
            }
            else
            {
                var filePath = EditorUtility.OpenFilePanel("Open Blend Shape JSON", "", "json");
                if (filePath.Length != 0)
                {
                    string jsonString = Util.ReadFromFile(filePath);
                    // Ugh... the JsonUtility doesn't support "non objects" (IE Arrays) as top level objects
                    RootObject jsonData = JsonUtility.FromJson<RootObject>(
                        "{\"shapeKeys\":" + jsonString + "}"
                    );
                    foreach (ShapeKey shapeKey in jsonData.shapeKeys)
                    {
                        int index = An.Util.getBlendShapeIndex(obj, shapeKey.name);
                        if (index > -1)
                        {
                            skinnedMeshRendererDst.SetBlendShapeWeight(index, shapeKey.value);
                        }
                    }
                }
            }
        }

        public static void ExportJson(GameObject obj, bool tryChildren)
        {
            string srcPath = An.Util.GetGameObjectPath(obj);
            if (!obj.TryGetComponent<SkinnedMeshRenderer>(out var skinnedMeshRendererDst))
            {
                if (tryChildren)
                {
                    foreach (Transform child in obj.transform)
                    {
                        ExportJson(child.gameObject, false);
                    }
                }
                else
                {
                    Debug.LogWarning(srcPath + " doesn't contain blend shapes!");
                }
            }
            else
            {
                var filePath = EditorUtility.SaveFilePanel(
                    "Save blend shapes as JSON",
                    "",
                    obj.transform.name.ToLower() + ".json",
                    "json"
                );

                if (filePath.Length != 0)
                {
                    // Fixme: This should really use the serializer rather than manually build a JSON string
                    string data;
                    string[] destBlendShapes = An.Util.getBlendShapeNames(obj);
                    bool hasData = false;
                    data = "[";
                    int count = 1;
                    int itemCount = An.Util.getBlendShapeNames(obj).Length;
                    foreach (string blendShapeName in An.Util.getBlendShapeNames(obj))
                    {
                        if (Array.Exists(destBlendShapes, element => element == blendShapeName))
                        {
                            int index = An.Util.getBlendShapeIndex(obj, blendShapeName);
                            if (index > -1)
                            {
                                float weightVal = An.Util.getBlendShapeWeightByName(
                                    obj,
                                    blendShapeName
                                );
                                data =
                                    data
                                    + "{"
                                    + "\"name\":\""
                                    + blendShapeName
                                    + "\""
                                    + ","
                                    + "\"value\":"
                                    + weightVal
                                    + "}";
                                if (count < itemCount)
                                {
                                    data = data + ",";
                                }
                                hasData = true;
                            }
                        }
                        count++;
                    }
                    data = data + "]";
                    if (hasData)
                        Util.WriteToFile(data, filePath);
                }
            }
        }
    }
}
#endif
